from django.shortcuts import render
from SWIMS.models import Librarian
# Create your views here.
def userdata(request):
		libq = Librarian.objects.values()
		return render(request, 'base_generic.html', {'libq':libq})
