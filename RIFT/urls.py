from django.contrib import admin
from django.urls import include, path
from RIFT import views
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
urlpatterns = [path('signup', views.signup, name = 'signup'),
path('submitted.html', TemplateView.as_view(template_name='submitted.html')),
path('signup.html', views.signup_list, name='signup_list'),
 path('export/csv/$', views.export_appointmentscsv, name='exportappointments'),
]
