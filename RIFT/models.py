from django.db import models
from SWIMS.models import Librarian
import django.utils.timezone
import datetime

# Create your models here.
class lform(models.Model):
	name = models.CharField(max_length=50, default="")
	email= models.CharField(max_length=50, default="")
	position = (
		('S', 'Student'),
		('F', 'Faculty'),
		('V', 'Visitor'),

		)
	status = models.CharField(max_length=1, choices=position, blank=True, default='S',)
	course = models.CharField(max_length=50, default="")
	day_and_time = models.CharField('date/time', max_length=50, default="")
	topic = models.CharField(max_length=50, default="")
	description = models.TextField(default = "")
	librarian = models.ForeignKey(Librarian, on_delete=models.CASCADE)
	

