from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from SWIMS.models import Librarian
from RIFT.models import lform
from django.urls import reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.shortcuts import redirect
from RIFT.forms import InstructionForm
from django.core.mail import send_mail
from django.core.mail import EmailMessage
import csv


def signup(request):

	form_inst=lform()

	if request.method == 'POST':
		form = InstructionForm(request.POST)
		if form.is_valid():

			form.save()
			#send_mail('Subject here', 'Here is the message.', settings.EMAIL_HOST_USER,
         #['dmoody2@aum.edu'], fail_silently=False)
			context={'form':form}#, "librariannum":librariannum}
			return render(request, 'submitted.html', context)
	else:
		form = InstructionForm(initial={})
		return render(request, 'sign_up.html', {'form': form, 'form_inst':form_inst,})
	 
	
def submitted(request):
	pass

def signup_list(request, **kwargs):
	current_user = request.user
	#name = request.user.last_name+","+request.user.first_name
	queryset = lform.objects.values()
	context = {'queryset':queryset, }
	return render(request,'listing.html',context)
	pass
	


def export_appointmentscsv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="appointmets.csv"'

    writer = csv.writer(response)
    writer.writerow(['Name', 'Email', 'Status', 'Course', 'Date/time', 'Topic' 'Description'])

    users = lform.objects.all().filter(librarian_id=request.user.id).values_list('name', 'email', 'status', 'course', 'day_and_time', 'topic', 'description')
    for user in users:
        writer.writerow(user)

    return response

