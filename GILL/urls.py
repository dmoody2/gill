"""GILL URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from SWIMS import views
from django.contrib.auth import views as auth_views
#from SWIMS.views import UserFormView
urlpatterns = [
 path('swims/', include('SWIMS.urls')),
    path('admin/', admin.site.urls),
    path('GILL', views.GillHome, name='GillHome')#some template page for all apps)
  #  path('/swims/tasks', views.TaskView, name="TaskView"),
   #    path('tasks/<int:id><int:assigned_by_id>', views.task_data, name="task_data")
]
urlpatterns += [
   path('accounts/', include('django.contrib.auth.urls')),
]

urlpatterns +=[
path('water/', include('WATER.urls')),
path('rift/', include('RIFT.urls')),
path('seas/', include('SEAS.urls')),

]


