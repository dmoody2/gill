from __future__ import division

from django.shortcuts import render
from .forms import evalform
from .models import mainform
from operator import itemgetter
from itertools import groupby
from django.db.models import Sum
from django.db.models import DateTimeField, ExpressionWrapper, F
import copy

def seeeval(request):
	if request.method == 'POST':
		form = evalform(request.POST)
		if form.is_valid():
			form.save()
			return render(request, 'sub.html', {'form':form})
	else:
		form = evalform()
		
	return render(request, 'eval.html', {'form':form})

def evalresults(request):
	librarian = request.user.id
	sessdata = []
	ids=[]
	aggregate=[]
	sessdivide = []
	divlist = []
	x=range(1,8)
	tot = 0
	people = []
	if librarian:
		sessiondata = mainform.objects.values('sessionid').filter(librarian_id = librarian).distinct() #grabs unique IDs
		sessiondata.order_by('sessionid')
		ids = [idnum['sessionid'] for idnum in sessiondata]	
		for i in ids:	
			for num in x:
				qlist = mainform.objects.filter(sessionid = i).aggregate(Sum('q'+str(num)))
				qlistdivide = mainform.objects.filter(sessionid = i).values('q'+str(num)).aggregate(QuestionAverage = (Sum('q'+str(num)) % 8) )
				sessdata.append(qlist)
				sessdivide.append(qlistdivide)
			aggregate.append(sessdata)
			divlist.append(sessdivide)
			sessdata = []
			sessdivide = []
			count = mainform.objects.values('q1').filter(librarian_id = librarian).filter(sessionid = i).count()
			people.append(count)
		aggregatedivide = copy.deepcopy(aggregate)
		for listing in aggregatedivide:
			for item in listing:
				for key, value in item.items():
					item[key]  = value/8
		zipped = zip(aggregate, aggregatedivide)
		mzip = zip(ids, people)
		peoplezip = zip(zipped,mzip)

		context={'sessiondata':sessiondata, 'sessdata':sessdata,'aggregate':aggregate, 'aggregatedivide':aggregatedivide,  'mzip':mzip, 'peoplezip':peoplezip
		
		}
		return render(request, 'results.html', context)
