from django import template
register = template.Library()

@register.filter
def div(value, div):
    return round((value / div) * 100, 2)
    
@register.filter(name='zip')
def zip_lists(a, b):
  return zip(a, b)


@register.filter(name='addition')
def count(num):
	num = num+1
	return num
	
@register.filter
def return_item(l, i):
    try:
        return l[i]
    except:
        return None

from django.template.defaulttags import register
...
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
