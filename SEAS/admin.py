from django.contrib import admin
from .models import period, course, mainform
# Register your models here.
admin.site.register(course)
admin.site.register(period)

admin.site.register(mainform)
