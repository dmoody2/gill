from django.db import models
from SWIMS.models import Librarian

	

class course(models.Model):
	name = models.CharField(default = "", max_length=50)
	def __str__(self): 
		return self.name 

class period(models.Model):
	name = models.CharField(default = "", max_length=50)
	def __str__(self): 
		return self.name 

class mainform(models.Model):
	librarian = models.ForeignKey(Librarian, on_delete=models.CASCADE)
	course = models.ForeignKey(course, on_delete=models.CASCADE)
	period = models.ForeignKey(period, on_delete=models.CASCADE)
	CHOICES = (('1','Strongly Disagree'),('2','Disagree'),('3','Agree'),('4','Strongly Agree'))
	q1 = models.CharField("The librarian was well prepared for this session", max_length = 50)
	q2 = models.CharField("The librarian seemed knowledgeable about the information sources covered", max_length = 50)
	q3 = models.CharField(  "After participating in the library session, I am more likely to use the library for my research", max_length = 50)
	q4 = models.CharField(  "As a result of this session, I know know what type of information can be found in the databases we visited", max_length = 50)
	q5 = models.CharField(  "As a result of this session, I know how to physically access and navigate the databases we visited", max_length = 50)
	q6 = models.CharField(  "As a result of this session, I feel confident that I can use library resources to find information for participation and questions", max_length = 50)
	q7 = models.CharField(  "The Librarian was friendly and encouraged participation and questions",max_length = 50)
	w1 = models.TextField(  "What did you find most useful?", max_length = 500)
	w2 = models.TextField(  "What changes, if any, would you recommend for this session?", max_length = 500)
	w3 = models.TextField(  "What would you like to have learned today that was not covered in this session?", max_length = 500)
	sessionid = models.CharField(max_length=80)
