from django import forms
from SWIMS.models import Librarian
from .models import period, course, mainform

#class mainform(forms.Form):
#	librarian = forms.ModelChoiceField(queryset = Librarian.objects.all())
#	course = forms.ModelChoiceField(queryset = course.objects.all())
#	period = forms.ModelChoiceField(queryset = period.objects.all())
#	CHOICES = (('1','Strongly Disagree'),('2','Disagree'),('3','Agree'),('4','Strongly Agree'))
#	q1 = forms.ChoiceField(label = "The librarian was well prepared for this session", widget=forms.RadioSelect, choices=CHOICES)
#	q2 = forms.ChoiceField(label = "The librarian seemed knowledgeable about the information sources covered", widget=forms.RadioSelect, choices=CHOICES)
#	q3 = forms.ChoiceField(label = "The librarian presented the material in a clear and understandable manner", widget=forms.RadioSelect, choices=CHOICES)
#	q4 = forms.ChoiceField(label = "After participating in the library session, I am more likely to use the library for my research", widget=forms.RadioSelect, choices=CHOICES)
#	q5 = forms.ChoiceField(label = "As a result of this session, I know know what type of information can be found in the databases we visited", widget=forms.RadioSelect, choices=CHOICES)
#	q6 = forms.ChoiceField(label = "As a result of this session, I know how to physically access and navigate the databases we visited", widget=forms.RadioSelect, choices=CHOICES)
#	q7 = forms.ChoiceField(label = "As a result of this session, I feel confident that I can use library resources to find information for participation and questions", widget=forms.RadioSelect, choices=CHOICES)
#	q8 = forms.ChoiceField(label = "The Librarian was friendly and encouraged participation and questions", widget=forms.RadioSelect, choices=CHOICES)
#	w1 = forms.CharField(widget=forms.Textarea, label = "What did you find most useful?", max_length = 500)
#	w2 = forms.CharField(widget=forms.Textarea, label = "What changes, if any, would you recommend for this session?", max_length = 500)
#	w3 = forms.CharField(widget=forms.Textarea, label = "What would you like to have learned today that was not covered in this session?", max_length = 500)

class evalform(forms.ModelForm):

	class Meta:
		CHOICES = (('1','Strongly Disagree'),('2','Disagree'),('3','Agree'),('4','Strongly Agree'))
		model = mainform
		fields = '__all__' 
		widgets = {'sessionid': forms.HiddenInput(),
		'q1':forms.RadioSelect(choices = CHOICES),
		'q2':forms.RadioSelect(choices = CHOICES),
		'q3':forms.RadioSelect(choices = CHOICES),
		'q4':forms.RadioSelect(choices = CHOICES),
		'q5':forms.RadioSelect(choices = CHOICES),
		'q6':forms.RadioSelect(choices = CHOICES),
		'q7':forms.RadioSelect(choices = CHOICES),
		'q8':forms.RadioSelect(choices = CHOICES),

		
		
		
		}
