from django.db import models
from django.contrib.auth.models import User
import django.utils.timezone
import datetime
from django.db.models.signals import *
from django.forms import ModelForm
class Librarian(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, primary_key=True)
	department = models.CharField(max_length=100)
	first_name = models.CharField(max_length=50, default = '')
	last_name = models.CharField(max_length=50, default = '')
	def __str__(self):
		return ",".join([self.last_name, " "+self.first_name])



class Student(models.Model):
	user  = models.ForeignKey(User, on_delete=models.CASCADE, primary_key=True)
	department = models.CharField(max_length=100)
	first_name = models.CharField(max_length=50, default = '')
	last_name = models.CharField(max_length=50, default = '')
	def __str__(self):
		return ",".join([self.last_name, " "+self.first_name])
	
			
class Task(models.Model):
	Student  = models.ForeignKey(Student, on_delete=models.CASCADE)
	task= models.CharField(max_length=20, default="")
	description=models.TextField(max_length=200, default="")
	assign_date = models.DateTimeField('date assigned')
	def __str__(self):
		return self.task
	assigned_by=models.ForeignKey(Librarian, on_delete=models.CASCADE, default=" ")
	completed = models.BooleanField(default=False)
	def save(self, **kwargs):
        # If Task is being marked complete, set the completed_date
		if self.completed:
			self.completed_date = datetime.datetime.now()
		super(Task, self).save()
	class Meta:
		permissions = (("can_mark_completed", "Set task as completed"),) 

