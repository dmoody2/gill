from django import forms
from django.forms import ModelForm
from SWIMS.models import Task
#from django.contrib.auth.models import User
from django import forms
import django.utils.timezone
import datetime
from django.db import models
from django.contrib.admin import widgets


from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _  

  
class MarkCompleted(forms.Form):
	completed=forms.BooleanField()
	def clean_completed(self):
		data = self.cleaned_data['completed']
        #Remember to always return the cleaned data.
		return data

#class UserForm(forms.ModelForm):
#	password = forms.CharField(widget=forms.PasswordInput)
#	class Meta:
#		model = User
#		fields = ["username", 'email', "password"]

class AssignForm(forms.ModelForm):
	class Meta:
		model = Task
		fields = '__all__'
	#	widgets = {'assign_date': widgets.AdminDateWidget()}
	#def __init__(self, *args, **kwargs):
	#	super(AssignForm, self).__init__(*args, **kwargs)
	#	self.fields['mydate'].widget = widgets.AdminDateWidget()
     #   self.fields['mytime'].widget = widgets.AdminTimeWidget()
      #  self.fields['a'].widget = widgets.AdminSplitDateTime()
