from django.apps import AppConfig


class SWIMSConfig(AppConfig):
    name = 'SWIMS'
