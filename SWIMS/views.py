from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Task, Student, Librarian
from django.urls import reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.shortcuts import render
from django.shortcuts import redirect
from .forms import AssignForm

def tasks(request):
	current_user = request.user
	queryset= Task.objects.filter(Student_id=request.user.id).filter()
	incompleteset= Task.objects.filter(completed=False).filter(Student_id=request.user.id)
	context={'queryset':queryset, 'incompleteset':incompleteset}
	return render(request,'index.html', context)

from django.shortcuts import get_object_or_404
import datetime

from .forms import MarkCompleted

def taskview(request, id, assigned_by_id, Student_id):
	task_instance=get_object_or_404(Task, id=id, assigned_by_id=assigned_by_id, Student_id=Student_id)
    # If this is a POST request then process the Form data
	if request.method == 'POST':
		form = MarkCompleted(request.POST)
        # Check if the form is valid:
		if form.is_valid():
			task_instance.completed = form.cleaned_data['completed']
			task_instance.save()
			return render(request, 'index.html')
	else:
   
		form = MarkCompleted(initial={'completed': True,})

	return render(request, 'SWIMS/markcomplete.html', {'form': form, 'taskinst':task_instance})
	
	
def GillHome(request):
	return render(request,'GillHome.html')
	

def task_assign(request):
	libq = Librarian.objects.values()
	if request.method == 'POST':
		form = AssignForm(request.POST)
		if form.is_valid:
			form.save()
			return render(request, 'subbed.html'  )
	else:
		form = AssignForm()
		return render(request, 'assign.html' , {'form':form, 'libq':libq})

