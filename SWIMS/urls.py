from django.urls import path

from SWIMS import views


urlpatterns = [
    path('tasks', views.tasks, name="tasks"),
    path('tasks/<int:id><int:assigned_by_id><int:Student_id>', views.taskview, name='taskview'),
	path('assign', views.task_assign, name="task assign")
	]
