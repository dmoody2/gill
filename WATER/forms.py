from django import forms
from WATER.models import Event, signup
from django.forms import ModelForm


class RegForm(ModelForm):
	class Meta:
		model=signup
		fields= ['Name','Email','Year']


class MakeEvent(ModelForm):
	class Meta:
		model = Event
		fields = "__all__"
