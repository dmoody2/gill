from django.db import models
# Create your models here.
class Event(models.Model):
	event_name = models.CharField(max_length=50, default="")
	event_location = models.CharField(max_length=20, default="")
	event_time = models.DateTimeField('date of event')
	event_topics = models.CharField(max_length=50, default="")
	event_description = models.TextField(max_length=500, default="")
	def __str__(self):
		return self.event_name

class signup(models.Model):
	Event = models.ForeignKey(Event, on_delete=models.CASCADE)
	Name = models.CharField(max_length=50, default="")
	Email= models.CharField(max_length=50, default="")
	YEAR = (
		('F', 'Freshman'),
		('S', 'Sophomore'),
		('J', 'Junior'),
		('Sr', 'Senior'),
		('G', 'Grad Student'),
		)

	Year = models.CharField(max_length=1, choices=YEAR, blank=True, default='F',)
	def __str__(self):
		return ",".join([self.Name, " "+self.Email])
	
