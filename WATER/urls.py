from WATER import views
from django.urls import path


urlpatterns = [

	path('events', views.events_view, name="events_view"),
    path('events/<int:id>', views.SingleEventData, name='SingleEventData'),
    path('events/<int:id>/signups', views.SeeRegistrants ,name="SeeRegistrants"),
    path('events/<int:id>/export', views.export_registrants, name='exportreg'),
path('eventbuild',views.makeevent, name = "eventbuild")

]

