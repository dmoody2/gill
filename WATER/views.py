from django.shortcuts import render
from WATER.models import Event, signup
from SWIMS.models import Librarian
from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from WATER.forms import RegForm, MakeEvent

# Create your views here.

def events_view(request):
	queryset=Event.objects.all()
	context={'queryset':queryset}
	return render(request,'events_listing.html', context)


def SingleEventData(request, id):
	queryset=Event.objects.filter(id=id).values()
	EvInst=signup()
	if request.method == 'POST':
		form = RegForm(request.POST)
		if form.is_valid():
			EvInst.Event_id=id
			EvInst.Name = form.cleaned_data['Name']
			EvInst.Email = form.cleaned_data['Email']
			EvInst.Year = form.cleaned_data['Year']
			EvInst.save()
			return render(request, 'event_detail.html')
	else:
		form = RegForm(initial={})

	return render(request, 'event_detail.html', {'form': form, 'EvInst':EvInst, 'queryset':queryset, 'id':id})
	
	
def SeeRegistrants(request, id):
	queryset = signup.objects.filter(Event_id=id)
	context={'queryset':queryset, 'id':id}
	return render(request, 'seeregistrants.html', context)
	
import csv
	
def export_registrants(request, id):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="registrants.csv"'
    writer = csv.writer(response)
    writer.writerow(['Name', 'Email', 'Year'])
    signsups = signup.objects.filter(Event_id=id).values_list('Name', 'Email', 'Year')
    for e in signsups:
        writer.writerow(e)

    return response

def makeevent(request):
	libq = Librarian.objects.values()
	if request.method == 'POST':
		form = MakeEvent(request.POST)
		if form.is_valid:
			form.save()
			return render(request, 'created.html'  )
	else:
		form = MakeEvent()
		return render(request, 'createevent.html' , {'form':form, 'libq':libq})
